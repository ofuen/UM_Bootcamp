module.exports = function(sequelize, DataTypes) {
  // Add code here to create a Post model
  // This model needs a title, a body, and a category
  // Don't forget to 'return' the post after defining

  // Sequelize (capital) references the standard library
  var Sequelize = require("sequelize");
  // sequelize (lowercase) references our connection to the DB.
  var sequelize = require("../config/connection.js");

  // Creates a "Character" model that matches up with DB
  var Character = sequelize.define("character", {
    // the routeName gets saved as a string
    routeName: Sequelize.STRING,
    // the name of the character (a string)
    title: Sequelize.STRING,
    // the character's role (a string)
    body: Sequelize.STRING,
    // the character's age (a string)
    category: Sequelize.INTEGER,
    // and the character's force points (an int)
    forcePoints: Sequelize.INTEGER
  });

  // Syncs with DB
  Character.sync();

  // Makes the Character Model available for other files (will also create a table)
  module.exports = Character;
};
